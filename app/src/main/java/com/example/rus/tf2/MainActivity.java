package com.example.rus.tf2;

import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private MyCustomViewGroup topViewGroup, bottomViewGroup;
    private List<MetroStation> metroStationList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        metroStationList = generateMetroStations();

        init();
    }

    private List<MetroStation> generateMetroStations() {
        List<MetroStation> generatedList = new ArrayList<>();
        generatedList.add(new MetroStation("Сокольники", Color.RED));
        generatedList.add(new MetroStation("Фрунзенская", Color.RED));
        generatedList.add(new MetroStation("Водный стадион", ContextCompat.getColor(this, R.color.colorGreen)));
        generatedList.add(new MetroStation("Ховрино", ContextCompat.getColor(this, R.color.colorGreen)));
        generatedList.add(new MetroStation("Сокол", ContextCompat.getColor(this, R.color.colorGreen)));
        generatedList.add(new MetroStation("Павелецкая", ContextCompat.getColor(this, R.color.colorGreen)));
        generatedList.add(new MetroStation("Белорусская", ContextCompat.getColor(this, R.color.colorGreen)));
        generatedList.add(new MetroStation("Курская", ContextCompat.getColor(this, R.color.colorGreen)));
        generatedList.add(new MetroStation("Бауманская", Color.BLUE));
        generatedList.add(new MetroStation("Электрозаводская", Color.BLUE));
        generatedList.add(new MetroStation("Аэропорт", ContextCompat.getColor(this, R.color.colorGreen)));
        generatedList.add(new MetroStation("Парк победы", Color.BLUE));
        generatedList.add(new MetroStation("Митино", Color.BLUE));
        generatedList.add(new MetroStation("Мякинино", Color.BLUE));
        generatedList.add(new MetroStation("Строгино", Color.BLUE));
        generatedList.add(new MetroStation("Молодежная", Color.BLUE));
        generatedList.add(new MetroStation("Смоленская", Color.BLUE));
        generatedList.add(new MetroStation("Щелковская", Color.BLUE));
        generatedList.add(new MetroStation("Первомайская", Color.BLUE));
        generatedList.add(new MetroStation("Партизанская", Color.BLUE));
        return generatedList;
    }


    private void init() {
        topViewGroup = findViewById(R.id.top_view_group);
        bottomViewGroup = findViewById(R.id.bottom_view_group);

        for (MetroStation metroStation: metroStationList){

            final LinearLayout linearLayout = (LinearLayout) getLayoutInflater().inflate(R.layout.metro_station_item, null, false);
            TextView textView = linearLayout.findViewById(R.id.metro_station_text_view);
            textView.setText(metroStation.getName());
            textView.setTextColor(metroStation.getColor());

            switch (metroStation.getColor()){
                case Color.RED:
                    textView.setCompoundDrawablesWithIntrinsicBounds(getDrawable(R.drawable.ic_dot_red), null, getDrawable(R.drawable.ic_cross_red),null);
                    break;
                case Color.BLUE:
                    textView.setCompoundDrawablesWithIntrinsicBounds(getDrawable(R.drawable.ic_dot_blue), null, getDrawable(R.drawable.ic_cross_blue),null);
                    break;
                default:
                    textView.setCompoundDrawablesWithIntrinsicBounds(getDrawable(R.drawable.ic_dot_green), null, getDrawable(R.drawable.ic_cross_green),null);
                    break;
            }

            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    replaceView(linearLayout);
                }
            });
            topViewGroup.addView(linearLayout);
        }

    }

    private void replaceView(final View linearLayout) {
        if (linearLayout.getParent() == topViewGroup){
            topViewGroup.removeView(linearLayout);
            bottomViewGroup.addView(linearLayout);
            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    replaceView(linearLayout);
                }
            });
        } else {
            bottomViewGroup.removeView(linearLayout);
            topViewGroup.addView(linearLayout);
            linearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    replaceView(linearLayout);
                }
            });
        }

    }
}
