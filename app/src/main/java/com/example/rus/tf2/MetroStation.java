package com.example.rus.tf2;

import android.graphics.Color;

public class MetroStation {

    private String name;
    private int color;

    public MetroStation(String name, int color) {
        this.name = name;
        this.color = color;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
